package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.example.model.CourseModel;

@Mapper
public interface CourseMapper
{
	@Select("select id_course, nama, sks from course where id_course = #{id_course}")
	@Results(value = {
			@Result(property = "idCourse", column = "id_course"),
			@Result(property = "nama", column = "nama"),
			@Result(property = "sks", column = "sks")})
	CourseModel selectCourse(@Param("id_course") String id_course);

	
	@Select("select id_course, nama, sks from course")
	@Results(value = {
			@Result(property = "idCourse", column = "id_course"),
			@Result(property = "nama", column = "nama"),
			@Result(property = "sks", column = "sks") })
    List<CourseModel> selectAllCourses ();
}
